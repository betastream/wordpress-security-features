<?php
/*
 * Plugin Name: beta-wp-security
 * Plugin URI: http://beta.io
 * Description: Betastream's WordPress Security
 * Author: Betastream
 * Author URI: http://beta.io
 * Version: 1.0
 * Text Domain: beta-wp-security
 * License: GPLv2
*/

/*
 * Re-create the iThemes functionality that we like/use
 * 	https://downloads.wordpress.org/plugin/better-wp-security.5.1.1.zip
 * Add IP/IP range blacklisting
 * 	Support blocklist format? (http://sourceforge.net/p/peerguardian/wiki/dev-blocklist-format-p2p/)
 * After X failed login attempt, notify and/or lock account
 * Provide password feature (strength feedback, expiration policy, etc)
 * Provide more secure authentication (using admission.io, 2FA, Google Authenticator/SecurID, SMS)
 * Add triggering of alarms/sending SNS topics
 */
define('BETA_WPSEC_DIR', dirname(__FILE__));

$locale = apply_filters( 'plugin_locale', get_locale(), 'beta-wp-security' );
load_textdomain( 'beta-wp-security', WP_LANG_DIR . "/plugins/beta-wp-security/lang/beta-wp-security-$locale.mo" );
load_plugin_textdomain( 'beta-wp-security' );

if ( is_admin() ) {
	// Do admin things
}

new \Betastream\WP_Security( __FILE__, __( 'beta-wp-security', 'beta-wp-security' ) );
