<?php
/**
 * This will lock an IP out
 * @package beta-wp-security
 * @author Pier-Luc Lafleur <pl@beta.io>
 * @version 1.0
 * @subpackage blacklist
 */


add_action('init', function(){
  $blacklisted = file_get_contents(WP_CONTENT_DIR.'/uploads/blacklist'); # Format?
  $ip = $_SERVER['X_HTTP_FORWARDED_FOR']? $_SERVER['X_HTTP_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
  if (in_array($ip, $blacklisted)) {
    header("HTTP/1.1 401 Unauthorized");
    exit;
  }
});
