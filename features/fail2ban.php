<?php
/**
 * This will lock a user/an IP out after a given amount of failed logins
 * @package beta-wp-security
 * @author Pier-Luc Lafleur <pl@beta.io>
 * @version 1.0
 * @subpackage fail2ban
 */

add_action('wp_login_failed', function(){
  // Options that should be customizable:
  if (is_user_logged_in()) return;
  $ttl = 300;
  // Okay
  $ip = $_SERVER['X_HTTP_FORWARDED_FOR']? $_SERVER['X_HTTP_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
  $trName = "beta_wpsec:tr:$ip";
  $tr = (int)get_transient($trName);
  if (!empty($tr)) {
    if ($tr >= BETA_WPSEC_MAX_FAILED_LOGINS) {
      // Bad boy, lock em out
      // How?
    }
    else {
      set_transient($trName, ++$tr, $ttl);
    }
  }
});
